import './scss/main.scss'
import './index.html'

import './js/animateScroll'
import './js/closeMenuToggle'

import './js/sdk-facebook'

import './js/activeMenuService'
import './js/stickyMenu'
import './js/smoothScroll'

import './js/slider'
