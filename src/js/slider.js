import Glide from '@glidejs/glide'

new Glide('.glide', {
  type: 'carousel',
  autoplay: 4000
}).mount()
