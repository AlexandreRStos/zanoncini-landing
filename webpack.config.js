const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
// const ImageSpritePlugin = require('image-sprite-webpack-plugin')

const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
  entry: path.resolve(__dirname, 'src/app.js'),
  output: {
    filename: devMode ? 'bundle.js' : 'bundle-[hash:8].js',
    path: path.resolve(__dirname, 'build')
  },
  devtool: 'source-map',
  devServer: {
    hot: devMode,
    contentBase: path.resolve(__dirname, 'src')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          { loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        ]
      },
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader',
          options: {
            minimize: true,
            attrs: ['img:src', 'source:srcset']
          }
        }
      },
      {
        test: /\.(png|jpg|jpeg|svg|woff2?|ttf|otf|eot)$/,
        exclude: /node_modules/,
        use: [
          { loader: 'file-loader',
            options: {
              name: devMode ? '[name].[ext]' : '[name]-[hash:8].[ext]',
              useRelativePath: true
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          { loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader },
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'postcss-loader', options: { sourceMap: true, importLoaders: 1 } },
          { loader: 'resolve-url-loader' },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules']
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: devMode ? '[name].[ext]' : '[name]-[hash:8].css',
      chunckFilename: devMode ? '[id].css' : '[id]-[hash:8].css'
    }),
    new OptimizeCssAssetsPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
      minify: {
        removeComments: !devMode,
        collapseWhitespace: !devMode
      }
    }),
    new webpack.HotModuleReplacementPlugin()
  ]
}
